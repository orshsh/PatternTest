public class Main {
    public static void HouseInfo(House house)
    {
        System.out.println("======================================================================");
        System.out.println(house);
        System.out.printf("Model: %s made of %s, %s floors\n", house.getModel(), house.getMaterial(), house.getFloors());
        System.out.printf("Heater: %s brand %s kwt by %s\n", house.getHeater().getBrand(), house.getHeater().getPowerkvt(), house.getHeater().getFuelType());
        System.out.printf("Tag: %s %s\n", house.getTag().getPrice(), house.getTag().getCurrencyType());
        System.out.println("======================================================================");
    }

    public static void StoveInfo(Stove stove)
    {
        System.out.printf("%s:  %s brand, %s burners by %s, has hood: %s \n", stove, stove.getBrand(), stove.getNumOfBurners(), stove.getFuelType(), stove.isHasHood());
    }

    public static void HeaterInfo(Heater heater)
    {
        System.out.printf("%s:  %s brand %s kwt by %s\n", heater, heater.getBrand(), heater.getPowerkvt(), heater.getFuelType());
    }
    public static void main(String[] args) {
        System.out.println("Two houses by Builder:");

        HouseBuilder builder = new LuxHouseBuilder();
        HouseDirector director = new HouseDirector(builder);
        House firsthouse = director.buildHouse();
        HouseInfo(firsthouse);

        HouseBuilder builder2 = new CheapHouseBuilder();
        HouseDirector director2 = new HouseDirector(builder2);
        House secondhouse = director2.buildHouse();
        HouseInfo(secondhouse);

        System.out.println("Equipment by Factory:");
        HeatingSystemsFactory factory = new LuxHSFactory();
        Stove firststove=factory.createStove();
        Heater firstheater=factory.createHeater();
        StoveInfo(firststove);
        HeaterInfo(firstheater);

        System.out.println();
        HeatingSystemsFactory factory1 = new EcoHSFactory();
        Stove secondstove=factory1.createStove();
        Heater secondheater=factory1.createHeater();
        StoveInfo(secondstove);
        HeaterInfo(secondheater);

    }
}