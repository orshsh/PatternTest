public class Heater {
    private String brand;
    private int powerkvt;
    private FuelType fuelType;

    public Heater(String brand, int powerkvt, FuelType fuelType)
    {
        this.brand = brand;
        this.powerkvt = powerkvt;
        this.fuelType = fuelType;
    }

    public String getBrand() {
        return brand;
    }

    public int getPowerkvt()
    {
        return powerkvt;
    }

    public FuelType getFuelType() {
        return fuelType;
    }
}
