public class HouseDirector {
        private HouseBuilder builder;

        public HouseDirector(HouseBuilder builder) {
            super();
            this.builder = builder;
        }
        public House buildHouse() {
            return builder.withModel().withFloors().withMaterial().withHeater().withTag().build();
        }
}
