public class LuxHouseBuilder implements HouseBuilder {
    private String model;
    private int floors;
    private Material material;
    private Heater heater;
    private Tag tag;

    public LuxHouseBuilder() {
        super();
    }

    @Override
    public HouseBuilder withModel() {
        this.model = "Classic of Lux";
        return this;
    }
    @Override
    public HouseBuilder withFloors() {
        this.floors = 2;
        return this;
    }
    @Override
    public HouseBuilder withMaterial(){
        this.material = Material.WOOD;
        return this;
    }
    @Override
    public HouseBuilder withHeater(){
        this.heater = new Heater("LuxHeat", 10, FuelType.METHANE);
        return this;
    }
    @Override
    public HouseBuilder withTag(){
        this.tag = new Tag(990000, CurrencyType.USD);
        return this;
    }
    @Override
    public House build() {
        House house = new House(model, floors, material, heater, tag);
        return house;
    }

}
