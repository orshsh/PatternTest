public interface HeatingSystemsFactory {
    Stove createStove();
    Heater createHeater();
}
