public class House {

        private String model;
        private int floors;
        private Material material;
        private Heater heater;
        private Tag tag;


    public House() {

        super();
    }

    public House(String model, int floors, Material material, Heater heater, Tag tag) {
        this();
        this.model = model;
        this.floors = floors;
        this.material = material;
        this.heater = heater;
        this.tag = tag;
    }


/*
        пример констр.
        public Car(String chassis, String body, String paint, String interior) {
            this();
            this.chassis = chassis;
            this.body = body;
            this.paint = paint;
            this.interior = interior;
        }
 */

    public String getModel() {return model;}

    public void setModel(String model) {
        this.model = model;
    }

    public int getFloors() {return floors;}

    public void setFloors(int floors) {
        this.floors = floors;
    }

    public Material getMaterial() {return material;}

    public void setMaterial(Material material) {this.material=material;}

    public Heater getHeater() {return heater;}

    public void setHeater (Heater heater) {this.heater=heater;}

    public Tag getTag() {return tag;}

    public void setTag (Tag tag) {this.tag=tag;}

    }