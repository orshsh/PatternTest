import java.util.Currency;

public class Tag {
    private int price;
    private CurrencyType currencyType;

    public Tag(int price, CurrencyType currencyType)
    {
        this.price = price;
        this.currencyType = currencyType;
    }

    public int getPrice() {
        return price;
    }

    public CurrencyType getCurrencyType() {
        return currencyType;
    }
}
