public class EcoHSFactory implements HeatingSystemsFactory {
    @Override
    public  Stove createStove()
    {return new Stove("Eco", false, 2, FuelType.ELECTRICITY);}
    @Override
    public Heater createHeater()
    {return new Heater("Eco", 1, FuelType.PELLETS);}
}
