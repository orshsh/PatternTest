public class LuxHSFactory implements HeatingSystemsFactory {
    @Override
    public  Stove createStove()
    {return new Stove("Very Lux", true, 6, FuelType.METHANE);}
    @Override
    public Heater createHeater()
    {return new Heater("Very Lux", 10, FuelType.PROPANE);}
}