public interface HouseBuilder {
        public HouseBuilder withModel();
        public HouseBuilder withFloors();
        public HouseBuilder withMaterial();
        public HouseBuilder withHeater();
        public HouseBuilder withTag();
        public House build();
}
