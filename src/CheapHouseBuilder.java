public class CheapHouseBuilder implements HouseBuilder {
        private String model;
        private int floors;
        private Material material;
        private Heater heater;
        private Tag tag;

        public CheapHouseBuilder() {
            super();
        }

        @Override
        public HouseBuilder withModel() {
            this.model = "Classic of Lux";
            return this;
        }
        @Override
        public HouseBuilder withFloors() {
            this.floors = 2;
            return this;
        }
        @Override
        public HouseBuilder withMaterial(){
            this.material = Material.WOOD;
            return this;
        }
        @Override
        public HouseBuilder withHeater(){
            this.heater = new Heater("Russian Pech", 1, FuelType.PELLETS);
            return this;
        }
        @Override
        public HouseBuilder withTag(){
            this.tag = new Tag(360000, CurrencyType.RUB);
            return this;
        }
        @Override
        public House build() {
            House house = new House("Econom Hata", 1, Material.FOAMBLOCK, heater, tag);
            return house;
        }
}
