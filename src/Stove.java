public class Stove {
        private String brand;

        public boolean hasHood;
        private int numOfBurners;
        private FuelType fuelType;

        public Stove(String brand, boolean hasHood, int numOfBurners, FuelType fuelType)
        {
            this.brand = brand;
            this.hasHood = hasHood;
            this.numOfBurners = numOfBurners;
            this.fuelType = fuelType;
        }

        public String getBrand() {
            return brand;
        }

        public boolean isHasHood() {return  hasHood;}

        public int getNumOfBurners()
        {
            return numOfBurners;
        }

        public FuelType getFuelType() {
            return fuelType;
        }
}
